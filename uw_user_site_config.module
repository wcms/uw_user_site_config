<?php

/**
 * @file
 * uw_user_site_config.module
 */

/**
 * Implements hook_permission().
 */
function uw_user_site_config_permission() {
  return array(
    'administer user site configuration' => array(
      'title' => t('Administer user site configuration'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu().
 */
function uw_user_site_config_menu() {
  $items['admin/config/system/user_site_configuration'] = array(
    'title' => 'Site configuration',
    'description' => 'Configure various settings about content types and other things that might be set by users.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uw_user_site_config_settings_form'),
    'access arguments' => array('administer user site configuration'),
  );
  return $items;
}

/**
 * Menu callback; Config form.
 */
function uw_user_site_config_settings_form() {
  /**
   * Get all config forms defined by hook_user_site_config()
   */
  $form = array();
  foreach (module_implements('user_site_config') as $module) {
    $module_code_orig = module_invoke($module, 'user_site_config');
    // Convert to array if necessary.
    if (!is_array($module_code_orig)) {
      $module_code_all[0] = $module_code_orig;
    }
    else {
      $module_code_all = $module_code_orig;
    }
    foreach ($module_code_all as $module_code) {
      if (is_object($module_code)) {
        // Get the title.
        if (isset($module_code->title)) {
          $module_name = $module_code->title;
        }
        else {
          // If one wasn't set, use the module name or the machine name if absolutely necessary.
          $module_info = system_get_info('module', $module);
          $module_name = isset($module_info['name']) ? $module_info['name'] : $module;
        }
        // If there is already a section with this title, modify the title as needed.
        $module_offset = 1;
        while (array_key_exists($module_name, $form)) {
          $module_offset++;
          $test_name = $module_name . ' (' . $module_offset . ')';
          if (!array_key_exists($test_name, $form)) {
            $module_name = $test_name;
          }
        }
        // Put the information provided by the module's hook into a fieldset.
        if ($module_code->form) {
          $form[$module_name] = array_merge(array(
            '#type' => 'fieldset',
            '#title' => t($module_name),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
          ), $module_code->form);
        }
      }
    }
  }
  if (!$form) {
    // If there is no form, tell people.
    $form['null'] = array(
      '#markup' => '<p>' . t('There is currently nothing to configure.') . '</p>',
    );
    return $form;
  }
  else {
    // Alphabetically sort the sections.
    ksort($form);
    return system_settings_form($form);
  }
}

/**
 *
 */
function uw_user_site_config_settings_form_validate(&$form, &$form_state) {
  /**
   * Get the name for all config forms defined by hook_user_site_config() so that error processing functions are run in the same order as they appear
   */
  $names = array();
  $offsets = array();
  foreach (module_implements('user_site_config') as $module) {
    $module_code_orig = module_invoke($module, 'user_site_config');
    // Convert to array if necessary.
    if (!is_array($module_code_orig)) {
      $module_code_all[0] = $module_code_orig;
    }
    else {
      $module_code_all = $module_code_orig;
    }
    foreach ($module_code_all as $offset => $module_code) {
      if (is_object($module_code)) {
        // Get the title.
        if (isset($module_code->title)) {
          $module_name = $module_code->title;
        }
        else {
          // If one wasn't set, use the module name or the machine name if absolutely necessary.
          $module_info = system_get_info('module', $module);
          $module_name = isset($module_info['name']) ? $module_info['name'] : $module;
        }
        // If there is already a section with this title, modify the title as needed.
        $module_offset = 1;
        while (array_key_exists($module_name, $names)) {
          $module_offset++;
          $test_name = $module_name . ' (' . $module_offset . ')';
          if (!array_key_exists($test_name, $names)) {
            $module_name = $test_name;
          }
        }
        $names[$module_name] = $module;
        $offsets[$module_name] = $offset;
      }
    }
  }

  if ($names) {
    // Alphabetically sort the sections.
    ksort($names);
    /**
     * Run the validations defined by hook_user_site_config_validation($form_state)
     */
    $implementers = module_implements('user_site_config_validate');
    foreach ($names as $name => $module) {
      if (in_array($module, $implementers)) {
        module_invoke($module, 'user_site_config_validate', $form_state, $offsets[$name]);
      }
    }
  }
}
