This module provides a single configuration page for things about the site that can be configured by privileged users (e.g. certain content type settings).

There is nothing on this configuration page by default. Modules can add content to this page using HOOK_user_site_config() and validate the submissions using HOOK_user_site_config_validate(). Note that the validation hook is optional; also, the validation hook will be ignored if the config hook doesn't return a form.

HOOK_user_site_config() should return an object, or an array of objects. Accepted properties are:
  name (optional) - if not provided, the name of the module providing the hook will be used
  form - a form array built using the form API
  
HOOK_user_site_config_validate($form_state, $offset) should perform form validation, and use the form_set_error() function when errors are found. The variables passed in are:
  $form_state - the data from the submitted form
  $offset - what loop through the array provided by HOOK_user_state_config() this validation request is coming from; use conditionals to only check the necessary fields

For example:

function uw_ct_home_page_banner_user_site_config() {
  $form['uw_banner_slideshow_max'] = array(
    '#type' => 'textfield',
    '#title' => t('Slideshow banners'),
    '#default_value' => variable_get('uw_banner_slideshow_max',8),
    '#description' => t('Enter the maximum number of banners to display in a slideshow, from 1 to 8. Any additional banners will be ignored. If fewer banners than this number exist, all banners will be displayed.'),
    '#size' => 2,
    '#required' => TRUE,
  );
  $config = new stdClass();
  $config->name = 'Home page banner settings';
  $config->form = $form;
  return $config;
}

function uw_ct_home_page_banner_user_site_config_validate($form_state, $offset) {
  if (!is_numeric($form_state['values']['uw_banner_slideshow_max']) || $form_state['values']['uw_banner_slideshow_max'] < 1 || $form_state['values']['uw_banner_slideshow_max'] > 8) {
    form_set_error('uw_banner_slideshow_max', t('The number of banners must be a number between 1 and 8.'));
  }
}
